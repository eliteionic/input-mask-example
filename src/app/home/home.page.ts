import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public masks = {
    phoneNumber: '(000) 000-0000',
    creditCard: '0000 0000 0000 0000',
  };

  constructor() {}
}
